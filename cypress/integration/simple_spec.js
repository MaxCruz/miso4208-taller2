describe('Los estudiantes', function() {

	it('Visits site and close popup', function() {
		cy.visit('https://losestudiantes.co')
		cy.contains('Cerrar').click()
	})

	it('Fails at login', function() {
		cy.contains('Ingresar').click()
		cy.get('.cajaLogIn').find('input[name="correo"]').click().type("wrongemail@example.com")
		cy.get('.cajaLogIn').find('input[name="password"]').click().type("12345")
		cy.get('.cajaLogIn').contains('Ingresar').click()
		cy.contains('El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.')
	})

        it('Try create registered account', function() {
		cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Max")
		cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Cruz")
		cy.get('.cajaSignUp').find('input[name="correo"]').click().type("mr.cruz@uniandes.edu.co")
                cy.get('.cajaSignUp').find('input[name="password"]').click().type("12345678")
		cy.get('.cajaSignUp').find('select[name="idUniversidad"]').select('Universidad de los Andes')
		cy.get('.cajaSignUp').find('select[name="idDepartamento"]').select('Ingeniería de Sistemas')
                cy.get('.cajaSignUp').find('input[name="acepta"]').click()
                cy.get('.cajaSignUp').contains('Registrarse').click()
                cy.contains('Error: Ya existe un usuario registrado con el correo \'mr.cruz@uniandes.edu.co\'')
		cy.contains('Ok').click()
	})

	it('Right login', function() {
		cy.get('.cajaLogIn').find('input[name="correo"]').click().clear()
		cy.get('.cajaLogIn').find('input[name="correo"]').click().type("mr.cruz@uniandes.edu.co")
		cy.get('.cajaLogIn').find('input[name="password"]').click().clear()
		cy.get('.cajaLogIn').find('input[name="password"]').click().type("l0535miso")
                cy.get('.cajaLogIn').find('.logInButton').click()
		cy.get('#cuenta').click()
	        cy.contains('Salir')
		cy.get('#cuenta').click()
	})

	it('Search teachers', function() {
		cy.get('.Select-placeholder').click()
		cy.get('.Select-input').find('input').type("disney", {force: true})
		cy.contains('Disney Rubiano')
	})

	it('Go to teacher page', function() {
	        cy.contains('Disney Rubiano').click()
		cy.get('h1.nombreProfesor').contains('Disney Rubiano')
	})

	it('Filter courses', function() {
	        cy.get('.materias').find('input[name="ISIS1404"]').click()
		cy.contains('en las materias seleccionadas')
	})

})

